#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <bits/stdc++.h> 
#include <sys/types.h>
#include <dirent.h>
#include <sys/wait.h>
#include <fcntl.h> 
#include <sys/stat.h> 
#include <fstream>
#include <fcntl.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <map>

#define VALID_DIR "validation"
#define WEIGHT_DIR "weight_vectors"
#define PIPE_NAME "myfifo"
#define CLASS_NUM 3

using namespace std;

struct file_dir{
    vector<string> files;
};

void ensembler();
void linear_classifier();
void voter();
file_dir search(string dir);
void write_in_named_pipe(const char* name, string message); 
string read_named_pipe();
vector<float> search_weights(string name);
vector<float> mult(vector<float>);
bool is_number(const std::string& s);
bool parse_input(istringstream& iss, vector<float> &weights, char delim);
vector<vector<float>> classifier_mult(vector<float> weights);
float class_mult(vector<float> data, vector<float> weights, int , int);
vector<int> detect_max_class(vector<vector<float>> data);
vector<string> encode_for_voter(vector<int> data);
int count_char(string data, char c); 
string split(string &input, char delim);
void add_to_map(map <int, int[CLASS_NUM]> &m, string data);
bool exists(map <int, int[CLASS_NUM]> m, const int key);
vector<tuple<int, int>> detect_max_classifier(map <int, int[CLASS_NUM]> m);
string encode_for_ensembler(vector<tuple<int, int>> data);
map<int, int> convert_to_map(string data);
float accuracy(map<int, int> m, vector<float> data);
vector<float> convert_file_to_map();

int main(int argc, char const *argv[])
{
    mkfifo(PIPE_NAME, 0666); 

    if (fork()==0)
        ensembler();

    if (fork()==0)
        voter();

    wait(NULL);
    wait(NULL);
    return 0;
}

bool linear_classifier(int pipe_desc[], int i){
    
    int mult = 0;
    char message[100];
    
    close(pipe_desc[2*i+1]);
    read(pipe_desc[2*i], message, 100);
    close(pipe_desc[2*i]);
   
    vector<int> max_classes;
    max_classes = detect_max_class(classifier_mult(search_weights(string(WEIGHT_DIR) + "/" + string(message))));
    vector<string> res = encode_for_voter(max_classes);
    for (string g: res){
        write_in_named_pipe(PIPE_NAME, g);
    }
    write_in_named_pipe(PIPE_NAME, "+,");
    
    _exit(EXIT_SUCCESS);
}

void ensembler(){
    file_dir names = search(WEIGHT_DIR);
    int number_of_files = names.files.size();
    int pipe_desc[2*number_of_files];

    for (size_t i = 0; i < number_of_files; i++)
    {
        pipe(&pipe_desc[2*i]);

        if (fork()==0)
            linear_classifier(pipe_desc, i);
        else
        {
            close(pipe_desc[2*i]);
            write(pipe_desc[2*i+1], names.files[i].c_str(), names.files[i].length()+1);
            close(pipe_desc[2*i+1]);
        }                
    }
    
    for (size_t i = 0; i < number_of_files; i++)
        wait(NULL);
    cout<<accuracy(convert_to_map(read_named_pipe()), convert_file_to_map())<<endl;
    exit(EXIT_SUCCESS);
}

void voter(){
    map<int, int[CLASS_NUM]> m;

    string data, d;
    int max_ID = -1, total_done=0;
    float max_val = -10;
    data = read_named_pipe();
    
    while(true)
    {
        add_to_map(m, data);
        total_done += count_char(data, '+');
        
        if (total_done==10)
            break;

        data = read_named_pipe();
        // cout<<"data="<<data<<endl;
    }
    write_in_named_pipe(PIPE_NAME, encode_for_ensembler(detect_max_classifier(m)));
    exit(EXIT_SUCCESS);
}

void write_in_named_pipe(const char* name, string message){
    int pipe_fd = open(name, O_WRONLY);
    write(pipe_fd, message.c_str(), message.length()+1);
    close(pipe_fd);
}

string read_named_pipe(){
    int pipe_fd = open(PIPE_NAME, O_RDONLY);
    char ch;
    string tmp="";
    while( tmp == "")
        while( read(pipe_fd, &ch,  1 > 0 )) 
            if (ch)
                tmp += ch;
    close(pipe_fd);
    return tmp;
}

file_dir search(string currFile){
    file_dir names;

    DIR *dir = opendir(currFile.c_str());
    struct dirent *entry = readdir(dir);

    while(entry != NULL){

        if(entry->d_type == DT_REG || 
            (entry->d_type == DT_DIR && 
            string(entry->d_name) != "." && string(entry->d_name) != "..") )
            names.files.push_back(entry->d_name);

        entry = readdir(dir);

    }

    return names;
}
vector<float> search_weights(string name){
    ifstream file (name);
    string line, id;
    vector<float> weights;

    if (!file.is_open())
        cerr<< "Error in openning file " << name <<endl;
    
    else
        while ( getline (file, line) )
        {
            istringstream iss(line);
            if(name[0]=='w'){
            
                parse_input(iss, weights, ',');
                parse_input(iss, weights, ',');
                parse_input(iss, weights, '\n');
            }
            else
            {
                parse_input(iss, weights, ',');
                if (parse_input(iss, weights, '\n'))
                    weights.push_back(1);
            }
            
        }
    file.close();
    return weights;
}

bool is_number(const std::string& s)
{
    return( strspn( s.c_str(), "-.0123456789" ) == s.size() );
}

bool parse_input(istringstream &iss, vector<float> &weights, char delim){
    string data;
    getline(iss, data, delim);
    if (is_number(data)){
        weights.push_back(stof(data));
        return true;
    }
    return false;
}

vector<vector<float>> classifier_mult(vector<float> weights){
    vector< vector<float> > res;
    vector<float> data;
    data = search_weights(string(VALID_DIR)+"/"+"dataset.csv");
    for(int j=0; j<data.size(); j+=3){
        vector<float> class_res;
        for (int i = 0; i < weights.size(); i+=CLASS_NUM)
            class_res.push_back(class_mult(data, weights, i, j));
        res.push_back(class_res);
    }
    return res;
}

float class_mult(vector<float> data, vector<float> weights, int w_ind, int data_ind){
    float res = 0;
    for (int i = 0; i < 3; i++)
        res += data[data_ind+i]*weights[w_ind+i];
    return res;
}

vector<int> detect_max_class(vector<vector<float>> data){
    vector<int> res;
    for (vector<float>d : data)
        res.push_back(max_element(d.begin(), d.end())-d.begin());
    
    return res;
}

vector<string> encode_for_voter(vector<int> data){
    vector <string> res;

    for(int i=0; i< data.size();i++)
        res.push_back(to_string(i) + ":" + to_string(data[i])+",");
    
    return res;
}   

int count_char(string data, char c){
    int count = 0;
    for(char ch: data)
        if(ch==c)
            count ++;
    return count;
}
string split(string &input, char delim){
   	istringstream iss(input);
    string s="";
    if (getline(iss, s, delim)){
        getline(iss, input);
        return s;
    }
}

void add_to_map(map <int, int[CLASS_NUM]> &m, string data){
    string line;
    string key;
    while(true){

        line = split(data, ',');
        key = split(line, ':');

        if(is_number(key) && is_number(line))
            m[stoi(key)][stoi(line)] +=1;

        if(data=="")
            break;
    }
}
bool exists(map <int, int[CLASS_NUM]> m, const int key)
{
    return m.find(key) != m.end();
}
vector<tuple<int, int>> detect_max_classifier(map <int, int[CLASS_NUM]> m){
    vector<tuple<int, int>> res;

    for (auto const& x : m){
        const int N = sizeof(x.second) / sizeof(int);
        res.push_back({x.first, distance(x.second, max_element(x.second, x.second + N))}); 
    }
    return res;
}
string encode_for_ensembler(vector<tuple<int, int>> data){
    string res="";

    for(auto x: data)
        res += to_string(get<0>(x)) + ":" + to_string(get<1>(x)) + ",";
    
    return res;
}

map<int, int> convert_to_map(string data){
    map<int, int> m;
    string line;
    string key;
    while(true){

        line = split(data, ',');
        key = split(line, ':');

        if(is_number(key) && is_number(line))
            m[stoi(key)] = stoi(line);

        if(data=="")
            break;
    }
    return m;
}
vector<float> convert_file_to_map(){
    ifstream file (string(VALID_DIR)+"/labels.csv");
    string line, data;
    vector<float> res;
    if (!file.is_open())
        cerr<< "Error in openning file " <<endl;
    
    else
        while ( getline (file, line) )
        {
            istringstream iss(line);
            parse_input(iss, res, '\n');
        }
    file.close();
    return res;
}
float accuracy(map<int, int> m, vector<float> data){
    int size=0 , correct=0;
    for(auto x: m){
        size++;
        if(x.second==data[x.first])
            correct++;
    }
    return (correct/(size*1.0)) * 100;
}